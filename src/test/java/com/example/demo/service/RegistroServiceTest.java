package com.example.demo.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.example.demo.modelo.Telefono;
import com.example.demo.modelo.Usuario;
import com.example.demo.modelo.dto.CrearUsuario;
import com.example.demo.repo.RegistroRepo;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class RegistroServiceTest{
	
	private static final String JWT = "eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJkZW1vLWJjaSIsImlhdCI6MTYwNTUxMDYyMCwiaWRVc3VhcmlvIjoiMzk4NDkxYWYtYzkyOC00ODk4LWE4MGUtMDRlNzEzODU1MGU0In0.BINaIDuxPQkGvXJX0gaCekV_-JcKDsJUVrR2mXcXQ1c";
	
	@InjectMocks
	private RegistroService registroService;
	
	@Mock
	private RegistroRepo registroRepo;
	
	@Mock
	private JwtService jwtService;
	
	private MockMvc mock;
	
	@Before
	public void config() throws IOException {
		MockitoAnnotations.initMocks(this);
		mock = MockMvcBuilders
				.standaloneSetup(registroService)
				.build();
	}
	
	@Test
	public void registrarUsuarioTest() {
		CrearUsuario crearUsuario = new CrearUsuario();
		crearUsuario.setEmail("olmedonicolas01@gmail.com");
		crearUsuario.setNombre("nicolás");
		crearUsuario.setPassword("1Asd2");
		crearUsuario.setFechaCreacion(LocalDateTime.now());
		Telefono telefono = new Telefono();
		telefono.setCodigoPais("+56");
		telefono.setCodigoCiudad("9");
		telefono.setNumero("91361566");
		List<Telefono> telefonos = new ArrayList<>();
		telefonos.add(telefono);
		crearUsuario.setTelefonos(telefonos);
		Usuario usuario = Usuario
				.builder()
				.email(crearUsuario.getEmail())
				.fechaCreacion(crearUsuario.getFechaCreacion())
				.nombre(crearUsuario.getNombre())
				.password(crearUsuario.getPassword())
				.isActive(true)
				.build();
		doReturn(usuario).when(registroRepo).save(Mockito.any());
		doReturn(usuario).when(registroRepo).findByEmail(crearUsuario.getEmail());
		Usuario usuarioRegistrado = registroService.registrarUsuario(crearUsuario);
		assertNotNull(usuarioRegistrado);
	}
	
	@Test
	public void doLoginTest() {
		String correo = "olmedonicolas01@gmail.com";
		Usuario usuario = Usuario
				.builder()
				.id(UUID.randomUUID().toString())
				.email(correo)
				.fechaCreacion(LocalDateTime.now())
				.nombre("nicolás")
				.password("1Asd2")
				.isActive(true)
				.build();
		doReturn(usuario).when(registroRepo).findByEmail(correo);
		doReturn(usuario).when(registroRepo).save(Mockito.any());
		doReturn(JWT).when(jwtService).emitToken(Mockito.any());
		Usuario usuarioLogueado = registroService.doLogin(correo);
		assertNotNull(usuarioLogueado);
		assertEquals(JWT, usuarioLogueado.getToken());
	}
}
