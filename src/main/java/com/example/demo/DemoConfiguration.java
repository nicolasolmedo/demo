package com.example.demo;

import java.util.Properties;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

@Configuration
public class DemoConfiguration {

	@Bean
	public HttpComponentsClientHttpRequestFactory httpFactory() {
		HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
		CloseableHttpClient httpClient = httpClientBuilder.build();
		return new HttpComponentsClientHttpRequestFactory(httpClient);
	}


	@Bean
	public ReloadableResourceBundleMessageSource messageSource() {
		ReloadableResourceBundleMessageSource bundle = new ReloadableResourceBundleMessageSource();
		bundle.setDefaultEncoding("UTF-8");
		Properties fileEncodings = new Properties();
		fileEncodings.setProperty("application-demo-message", "UTF-8");
		bundle.setFileEncodings(fileEncodings);
		bundle.setFallbackToSystemLocale(true);
		bundle.setBasename("classpath:application-demo-message");
		return bundle;
	}
}
