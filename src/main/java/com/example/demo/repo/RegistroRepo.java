package com.example.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.modelo.Usuario;

@Repository
public interface RegistroRepo extends JpaRepository<Usuario, String>{

	Usuario findByEmail(String email);

}
