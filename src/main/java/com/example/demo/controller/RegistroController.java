package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.modelo.Usuario;
import com.example.demo.modelo.dto.CrearUsuario;
import com.example.demo.modelo.dto.Login;
import com.example.demo.service.RegistroService;
import com.example.demo.utils.ErrorApi;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@RestController()
@Slf4j
public class RegistroController {

    @Autowired
    private RegistroService registroService;
    
    @ApiOperation(value = "login y obtiene un jwt para acceso api")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Usuario logueado", response = HttpStatus.class),
            @ApiResponse(code = 400, message = "Error en el ingreso de datos",response = HttpStatus.class)})
    @PostMapping(value = "/registro/login",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Usuario> login(@RequestBody @Validated Login login, BindingResult result){
    	if(result.hasErrors()) {
    		return new ResponseEntity(ErrorApi.objectErrorToList(result.getAllErrors()),HttpStatus.BAD_REQUEST);
    	}
    	Usuario usuario = registroService.doLogin(login.getEmail());
    	return new ResponseEntity<>(usuario,HttpStatus.OK);
    }

    @ApiOperation(value = "Crea usuarios que no esten en bd")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Usuario creado", response = HttpStatus.class),
            @ApiResponse(code = 400, message = "Error en el ingreso de datos",response = HttpStatus.class),
            @ApiResponse(code = 409, message = "Error al guardar usuario", response = HttpStatus.class)})
    @PostMapping(value = "/registro",consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Usuario> crearUsuario(@RequestBody @Validated CrearUsuario crearUsuario, BindingResult result){
        if(result.hasErrors()){
        	log.debug("No cumple con alguna validación");
            return new ResponseEntity(ErrorApi.objectErrorToList(result.getAllErrors()),HttpStatus.BAD_REQUEST);
        }
        Usuario usuario = registroService.registrarUsuario(crearUsuario);
        if(null == usuario){
        	log.debug("Se produjo algún error al intentar guardar usuario");
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(usuario,HttpStatus.CREATED);
    }
}
