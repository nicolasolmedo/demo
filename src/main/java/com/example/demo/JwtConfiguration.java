package com.example.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Configuration
public class JwtConfiguration {

	private static final String JWT_ALGORITHM = "demo.jwt.algorithm";
	private static final String JWT_SECRET = "demo.jwt.secret";
	private static final String JWT_ISSUER = "demo.jwt.issuer";
	
	@Bean
	public SignatureAlgorithm jwtAlgorithm(Environment env) {
		String strAlgorithm = env.getProperty(JWT_ALGORITHM,String.class,"NULL");
		return SignatureAlgorithm.forName(strAlgorithm);
	}

	@Bean
	public JwtBuilder jwtBuilder(Environment env) {		
		String jwtSecretString = env.getProperty(JWT_SECRET,String.class,"NULL");
		if (jwtSecretString.length() < 32) {
		}
		String jwtIssuerString = env.getProperty(JWT_ISSUER,String.class,"");
		if (jwtIssuerString.isEmpty()) {
		}
		return Jwts.builder().setIssuer(jwtIssuerString).signWith(jwtAlgorithm(env), jwtSecretString.getBytes());
	}
}
