package com.example.demo.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

public class ErrorApi {

	public static List<Map<String, String>> objectErrorToList(List<ObjectError> errores){
		List<Map<String,String>> mensajesError = new ArrayList<>();
		errores.forEach(e -> {
			FieldError campo = (FieldError)e;
			Map<String,String> error = new HashMap<String,String>();
			error.put(campo.getField(), e.getDefaultMessage());
			mensajesError.add(error);
		});
		return mensajesError;
	}
}
