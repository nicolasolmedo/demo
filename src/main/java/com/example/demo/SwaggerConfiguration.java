package com.example.demo;

import java.util.Locale;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import com.google.common.base.Predicates;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.DocExpansion;
import springfox.documentation.swagger.web.OperationsSorter;
import springfox.documentation.swagger.web.TagsSorter;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {
	
	private ApiInfo apiInfo(ReloadableResourceBundleMessageSource messageSource) {
		return new ApiInfoBuilder()
				.title(messageSource.getMessage("demo.titulo", null, Locale.getDefault()))
				.version(messageSource.getMessage("demo.api.version", null, Locale.getDefault()))
				.description("Demo para bci").build();
	}

	@Bean
	public Docket loginApi(ReloadableResourceBundleMessageSource messageSource) {

		return new Docket(DocumentationType.SWAGGER_2).groupName("demo").apiInfo(apiInfo(messageSource))
				.forCodeGeneration(true).select()
				.apis(Predicates.not(RequestHandlerSelectors.basePackage("org.springframework"))).build()
				.enableUrlTemplating(false)
				.useDefaultResponseMessages(false);
	}
    

	@Bean
	public UiConfiguration uiConfig() {		
		return UiConfigurationBuilder.builder()				
				.displayOperationId(true)
				.defaultModelsExpandDepth(1)
				.defaultModelExpandDepth(1)
				.docExpansion(DocExpansion.NONE)
				.filter(true)
				.displayRequestDuration(true)
				.maxDisplayedTags(null)
				.operationsSorter(OperationsSorter.METHOD)
				.showExtensions(false)
				.tagsSorter(TagsSorter.ALPHA)
				.supportedSubmitMethods(UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS).validatorUrl(null)
				.build();
	}

}
