package com.example.demo.validators;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.modelo.Usuario;
import com.example.demo.modelo.dto.CrearUsuario;
import com.example.demo.service.RegistroService;
import com.example.demo.validators.ValidaCrearUsuario.ValidaCrearUsuarioHelper;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Constraint(validatedBy = ValidaCrearUsuarioHelper.class)
public @interface ValidaCrearUsuario {

	String message() default "";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
	public class ValidaCrearUsuarioHelper
	implements ConstraintValidator<ValidaCrearUsuario, CrearUsuario> {
		
		@Autowired
		private RegistroService registroService;
		
		@Override
		public boolean isValid(CrearUsuario pojo, ConstraintValidatorContext ctx) {
			ctx.disableDefaultConstraintViolation();
			List<Boolean> errores = new ArrayList<>();
			errores.add(this.noExisteUsuario(pojo,ctx));
			return !errores.contains(false);
		}

		private boolean noExisteUsuario(CrearUsuario pojo,ConstraintValidatorContext ctx) {
			Usuario usuario = registroService.obtenerUsuario(pojo.getEmail());
			if(null != usuario){
				ctx
				.buildConstraintViolationWithTemplate("El Email ingresado ya esta en uso")
				.addPropertyNode("email")
				.addConstraintViolation();
				return false;
			}
			return true;
		}
	}
}
