package com.example.demo.validators;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.modelo.Usuario;
import com.example.demo.modelo.dto.Login;
import com.example.demo.service.RegistroService;
import com.example.demo.validators.ValidaLogin.ValidaLoginHelper;


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Constraint(validatedBy = ValidaLoginHelper.class)
public @interface ValidaLogin {

	String message() default "";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
	public class ValidaLoginHelper
	implements ConstraintValidator<ValidaLogin, Login> {
		
		@Autowired
		private RegistroService registroService;
		
		@Override
		public boolean isValid(Login pojo, ConstraintValidatorContext ctx) {
			ctx.disableDefaultConstraintViolation();
			List<Boolean> errores = new ArrayList<>();
			errores.add(this.validaLogin(pojo, ctx));
			return !errores.contains(false);
		}

		private boolean validaLogin(Login pojo,ConstraintValidatorContext ctx) {
			Usuario usuario = registroService.obtenerUsuario(pojo.getEmail());
			if(usuario == null || !usuario.getPassword().equals(pojo.getPassword())){
				ctx
				.buildConstraintViolationWithTemplate("Usuario o contraseña incorrectos")
				.addPropertyNode("email")
				.addConstraintViolation();
				return false;
			}
			return true;
		}
	}
}
