package com.example.demo.service;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.JwtBuilder;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class JwtService {

	@Autowired
	private JwtBuilder jwtBuilder;
	public String emitToken(Map<String, Object> claims) {
		log.debug("GENERAR TOKEN JWT A PARTIR DE CLAIMS");
		Calendar date = Calendar.getInstance();
		Date fechaCreacion = new Date(date.getTimeInMillis());
		return jwtBuilder.setIssuedAt(fechaCreacion).addClaims(claims).compact();
	}
}
