package com.example.demo.service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.modelo.Usuario;
import com.example.demo.modelo.dto.CrearUsuario;
import com.example.demo.repo.RegistroRepo;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RegistroService {

	@Autowired
	private RegistroRepo registroRepo;
	
	@Autowired
	private JwtService jwtService;

    public Usuario registrarUsuario(CrearUsuario crearUsuario) {
    	log.debug("EN METODO REGISTRAR USUARIO");
        Usuario usuario = Usuario
        					.builder()
        					.email(crearUsuario.getEmail())
        					.fechaCreacion(crearUsuario.getFechaCreacion())
        					.nombre(crearUsuario.getNombre())
        					.password(crearUsuario.getPassword()) //debería encriptarla pero para el ejemplo no lo hice
        					.isActive(true)
        					.build();
        if(null != crearUsuario.getTelefonos()) {
        	usuario.setTelefonos(crearUsuario.getTelefonos());
        }
        registroRepo.save(usuario);
        log.debug("USUARIO GUARDADO Y SE RETORNA");
        return this.obtenerUsuario(crearUsuario.getEmail());
    }
    
    public Usuario obtenerUsuario(String email){
    	log.debug("OBTENGO USUARIO POR CORREO");
    	return registroRepo.findByEmail(email);
    }
    
    public String generarTokenJwt(String idUsuario) {
    	log.debug("GENERA CLAIMS PARA EL TOKEN JWT");
    	Map<String,Object> claims = new HashMap<>();
    	claims.put("idUsuario", idUsuario); //a modo de ejemplo, podría ser cualquier otro dato
    	return jwtService.emitToken(claims);
    }
    
    public Usuario doLogin(String email) {
    	log.debug("EN METODO DOLOGIN");
    	log.debug("OBTENER USUARIO POR CORREO");
    	Usuario usuario = this.obtenerUsuario(email);
    	usuario.setUltimoLogin(LocalDateTime.now()); //no cambio la fecha de modificacion porque considero que debe cambiar cuando modifico algo del usuario
    	log.debug("GUARDAR ULTIMO LOGIN");
    	registroRepo.save(usuario);
    	usuario.setToken(this.generarTokenJwt(usuario.getId()));
    	return usuario;
    }
}
