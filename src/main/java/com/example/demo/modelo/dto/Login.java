package com.example.demo.modelo.dto;

import com.example.demo.validators.ValidaLogin;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@ValidaLogin
public class Login {

	String email;
	String password;
}
