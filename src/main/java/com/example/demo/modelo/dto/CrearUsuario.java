package com.example.demo.modelo.dto;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import com.example.demo.modelo.Telefono;
import com.example.demo.validators.ValidaCrearUsuario;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@ValidaCrearUsuario
public class CrearUsuario {

    @NotEmpty(message = "Debe indicar nombre")
    String nombre;
    @NotEmpty(message = "Debe indicar corro electrónico")
    @Pattern(regexp = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$",message = "Debe ingresar un correo electrónico valido")
    String email;
    @NotEmpty(message = "Debe indicar contraseña")
    @Pattern(regexp = "^(?=.*[0-9].*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z])[a-zA-Z\\d]{4,}$",message = "La contraseña debe tener dos números, al menos una mayúscula y al menos una minúscula")
    String password;
    List<Telefono> telefonos;
    LocalDateTime fechaCreacion = LocalDateTime.now();
}
