# Getting Started

### Como ejecutar este proyecto

0.- Descargar el proyecto

1.- Una vez descargado el proyecto, debes importar a tu IDE seleccionado (STS, Intellij u otro) indicando que es un proyecto "Gradle"

2.- Luego de importar comienza la descarga de todas las dependencias necesarias para el funcionamiento del proyecto por lo que tenemos que esperar

3.- Una vez descargadas, si utilizas STS va a aparecer el proyecto en un apartado que dice "boot dashborad" se puede seleccionar y presionar el boton de start.

3.1.- EN el caso de IntelliJ simplemente hacer clic en el botón de play

4.- Esperando que todo corriera sin problemas... iremos a http://localhost:8080/demo/swagger-ui.html

4.1.- Contiene una interfaz grafica para poder realizar las pruebas sin necesidad de alguna otra herramienta

5.- Seleccionamos registro-controller, se desplegarán dos submenu:

	* registro: Considerado para registrarse en el sistema, despliega la información necesaria para el registro y da aviso en caso de no cumplir con algun requisito minimo.
	
	* registro/login: Autenticación al sistema, se valida que el correo no exista en base de datos y que la contraseña sea la del usuario solicitado.
	
	* Al seleccionar uno, se despliega un botón "try it out" el cual al hacer clic nos permite editar el json de ejemplo, para ambos casos es necesario llenar el json con lo que se solicita y luego simplemente presionar en "Ejecutar" que es el botón azul que está más abajo.
	
6.- Dependiento de los datos ingresados en la sección "Server response" se mostrará el mensaje de exito con los datos devueltos por el servidor como también los mensajes de errores en caso de haber alguno.



### Detalles de alcance

En el pdf, indicaba que era un endpoint o al menos se da a entender eso. Lo que hice fue separar el registro del login.\
No estoy seguro de si ese es el formato que indicaban para los mensajes de errores pero lo entendí de esa forma en base al documento. Se puede cambiar desde la clase de utilidad. \
El token de jwt prefiero no guardarlo en bd y solo entregarlo cuando la persona hace login. \
agregué un @JsonIgnore en el get de contraseña para no exponer la contraseña cuando se hace login y cuando se crea el usuario \

Son algunos detalles que consideré distinto al documento. \